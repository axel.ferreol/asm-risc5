open Arith

module M = Necromonads.List
module IMap = Map.Make( Stdlib.Int )

module rec Types : sig
  type nat = int
  type ident = string
  type environment = int -> int
  type memory = int -> int Unspec(M)(Types).option
end = Types

module Spec = struct
  include Unspec (M) (Types)

  let env_init = fun x -> if x = 0 then 0 else 0
  let read_env (env,reg) = M.ret (env reg)
  let update_env (env,reg,n) =
    if reg = 0 
      then M.ret (env) 
      else M.ret (fun x -> if x = reg then n else env x)
  
  let mem_init = fun x -> None
  let read_mem (mem,addr) = M.ret (mem addr)
  let update_mem (mem,addr,n) =
    M.ret (fun x -> if x = addr then (Some n) else mem x)

  let iszero r = if r = 0 then M.ret() else M.fail()
  let isnzero r = if r != 0 then M.ret() else M.fail()
  let iseq (l,l') = if l = l' then M.ret() else M.fail()
  let isneq (l,l') = if l != l' then M.ret() else M.fail()
  let streq (l,l') = if l = l' then M.ret() else M.fail()
  let strneq (l,l') = if l != l' then M.ret() else M.fail()

  let add (x,y) = M.ret (x+y)
  let div (x,y) = M.ret (x/y)
  let xor (x,y) = M.ret (x lxor y)
end
  
open MakeInterpreter (Spec)

let string_of_tr_elt elt =
  match elt with
  | Tr_EnvUpdate(r,n) -> Printf.sprintf "x%d <- %d\n" (r) (n)
  | Tr_MemUpdate(addr,n) -> Printf.sprintf "@%d <- %d\n" (addr) (n)
  | Tr_Add(rd,rs1,rs2) -> Printf.sprintf "Add x%d, x%d, x%d :" (rd) (rs1) (rs2)
  | Tr_Xor(rd,rs1,rs2) -> Printf.sprintf "Xor x%d, x%d, x%d :" (rd) (rs1) (rs2)
  | Tr_Addi(rd,rs1,i) -> Printf.sprintf "Addi x%d, x%d, %d :" (rd) (rs1) (i)
  | Tr_Lw(rd,rs) -> Printf.sprintf "Lw x%d, x%d :" (rd) (rs)
  | Tr_Sw(rs,rd) -> Printf.sprintf "Sw x%d, x%d :" (rs) (rd)
  | Tr_AccessError(addr) -> Printf.sprintf "@! %d" (addr)
  | Tr_Label (l) -> Printf.sprintf "Label: %s\n" (l)
  | Tr_Pass (l) -> "" (* Printf.sprintf "No jump to: %s\n" (l) *)
  | Tr_JumpFail(l) -> Printf.sprintf "Label not found: %s\n" (l)
  | Tr_Jump (l) -> Printf.sprintf "Jump to: \"%s\"\n" (l)
  | Tr_Div(rd,rs1,rs2) -> Printf.sprintf "Div x%d, x%d, x%d :" (rd) (rs1) (rs2)
  | Tr_DivZero -> Printf.sprintf "Fail: division by zero\n"

let rec list_of_trace tr =
  match tr with
  | Nil_trace -> []
  | Sgl elt -> elt :: []
  | Conc (tr1,tr2) -> (list_of_trace tr1) @ (list_of_trace tr2)

let string_of_trace tr =
  Printf.sprintf "%s"
    (String.concat "" (List.map string_of_tr_elt (list_of_trace tr)))

let print_env env =
  let rec aux env reg reg_max=
    if reg < reg_max 
      then  let _ = Printf.printf "%d = %d\n" (reg) (env reg) in
            aux env (reg+1) reg_max 
    in aux env 0 3
    
let test prog =
  let (opt,tr) = M.extract(exec prog) in
  match opt with
  | None -> Printf.printf "Error \nProgram: \n%s\n"(string_of_trace tr)
  | Some (env,mem) ->
      let res = env 1 in
      print_env env ;
      Printf.printf "\n" ;
      Printf.printf "Program: \n%s\n\
        Result (x1) = %d\n\n" (string_of_trace tr) (res) 

let (@::) x y = Succ (x,y)

let mov (r1,r2) =
  Addi (r1,r2,Imm 0)
let dec (r) =
  Addi (r,r,Imm (-1))
let inc (r) =
  Addi (r,r,Imm (1))
let jump(l)=
  Beq (R 0, R 0, l)
let zero(r) =
  Xor (r,r,r)


let fibonaci n =
  Instr (zero (R 1)) @:: (*R1 : addr*)
  Instr (zero (R 2)) @:: (*R2 : value*)
  Instr (zero (R 3)) @:: (*R3 : n*)
  Instr (Addi (R 3, R 3, Imm n)) @::
  
  Instr (Sw (R 2, R 1)) @:: (*0 <- 0*)
  Beq (R 3, R 1, "result") @::
  Instr (inc(R 1)) @:: 
  Instr (inc(R 2)) @::
  Instr (Sw (R 2, R 1)) @:: (*1 <- 1*)
  
  Label "loop" @::
  Beq (R 3, R 1, "result") @::
  Instr (dec(R 1)) @::
  Instr (Lw (R 4, R 1)) @::
  Instr (inc (R 1)) @::
  Instr (Lw (R 5, R 1)) @::
  Instr (Add (R 5, R 5, R 4)) @::
  Instr (inc (R 1)) @::
  Instr (Sw (R 5, R 1)) @::
  jump ("loop") @::

  Label "result" @::
  Instr (Lw (R 2, R 1)) @::
  Instr (mov (R 1, R 2)) @::
  Nil

let () = test (fibonaci 14)
