(** This file was automatically generated using necroml
	See https://gitlab.inria.fr/skeletons/necro-ml/ for more informations *)

(** The unspecified types *)
module type TYPES = sig
  type environment
  type ident
  type memory
  type nat
end

(** The interpretation monad *)
module type MONAD = sig
  type 'a t
  val ret: 'a -> 'a t
  val bind: 'a t -> ('a -> 'b t) -> 'b t
  val branch: (unit -> 'a t) list -> 'a t
  val fail: unit -> 'a t
  val apply: ('a -> 'b t) -> 'a -> 'b t
  val extract: 'a t -> 'a
end

(** All types, and the unspecified terms *)
module type UNSPEC = sig
  module M: MONAD
  include TYPES

  type trace =
  | Conc of (trace * trace)
  | Sgl of tr_elt
  | Nil_trace
  and tr_elt =
  | Tr_Div of (nat * nat * nat)
  | Tr_DivZero
  | Tr_Jump of ident
  | Tr_JumpFail of ident
  | Tr_Pass of ident
  | Tr_Label of ident
  | Tr_AccessError of nat
  | Tr_MemUpdate of (nat * nat)
  | Tr_Sw of (nat * nat)
  | Tr_Lw of (nat * nat)
  | Tr_Addi of (nat * nat * nat)
  | Tr_Xor of (nat * nat * nat)
  | Tr_Add of (nat * nat * nat)
  | Tr_EnvUpdate of (nat * nat)
  and register =
  | R of nat
  and program =
  | Succ of (asm * program)
  | Nil
  and 'a option =
  | Some of 'a
  | None
  and instruction =
  | Sw of (register * register)
  | Lw of (register * register)
  | Addi of (register * register * immediate)
  | Xor of (register * register * register)
  | Div of (register * register * register)
  | Add of (register * register * register)
  and immediate =
  | Imm of nat
  and asm =
  | Bneq of (register * register * ident)
  | Beq of (register * register * ident)
  | Label of ident
  | Instr of instruction
  and monad = environment * memory -> value M.t
  and value = (environment * memory) option * trace

  val add: nat * nat -> nat M.t
  val div: nat * nat -> nat M.t
  val env_init: environment
  val iseq: nat * nat -> unit M.t
  val isneq: nat * nat -> unit M.t
  val isnzero: nat -> unit M.t
  val iszero: nat -> unit M.t
  val mem_init: memory
  val read_env: environment * nat -> nat M.t
  val read_mem: memory * nat -> (nat option) M.t
  val streq: ident * ident -> unit M.t
  val strneq: ident * ident -> unit M.t
  val update_env: environment * nat * nat -> environment M.t
  val update_mem: memory * nat * nat -> memory M.t
  val xor: nat * nat -> nat M.t
end

(** A default instantiation *)
module Unspec (M: MONAD) (T: TYPES) = struct
  exception NotImplemented of string
  include T
  module M = M

  type trace =
  | Conc of (trace * trace)
  | Sgl of tr_elt
  | Nil_trace
  and tr_elt =
  | Tr_Div of (nat * nat * nat)
  | Tr_DivZero
  | Tr_Jump of ident
  | Tr_JumpFail of ident
  | Tr_Pass of ident
  | Tr_Label of ident
  | Tr_AccessError of nat
  | Tr_MemUpdate of (nat * nat)
  | Tr_Sw of (nat * nat)
  | Tr_Lw of (nat * nat)
  | Tr_Addi of (nat * nat * nat)
  | Tr_Xor of (nat * nat * nat)
  | Tr_Add of (nat * nat * nat)
  | Tr_EnvUpdate of (nat * nat)
  and register =
  | R of nat
  and program =
  | Succ of (asm * program)
  | Nil
  and 'a option =
  | Some of 'a
  | None
  and instruction =
  | Sw of (register * register)
  | Lw of (register * register)
  | Addi of (register * register * immediate)
  | Xor of (register * register * register)
  | Div of (register * register * register)
  | Add of (register * register * register)
  and immediate =
  | Imm of nat
  and asm =
  | Bneq of (register * register * ident)
  | Beq of (register * register * ident)
  | Label of ident
  | Instr of instruction
  and monad = environment * memory -> value M.t
  and value = (environment * memory) option * trace

  let add _ = raise (NotImplemented "add")
  let div _ = raise (NotImplemented "div")
  let iseq _ = raise (NotImplemented "iseq")
  let isneq _ = raise (NotImplemented "isneq")
  let isnzero _ = raise (NotImplemented "isnzero")
  let iszero _ = raise (NotImplemented "iszero")
  let read_env _ = raise (NotImplemented "read_env")
  let read_mem _ = raise (NotImplemented "read_mem")
  let streq _ = raise (NotImplemented "streq")
  let strneq _ = raise (NotImplemented "strneq")
  let update_env _ = raise (NotImplemented "update_env")
  let update_mem _ = raise (NotImplemented "update_mem")
  let xor _ = raise (NotImplemented "xor")
end

(** The module type for interpreters *)
module type INTERPRETER = sig
  module M: MONAD

  type environment
  type ident
  type memory
  type nat

  type trace =
  | Conc of (trace * trace)
  | Sgl of tr_elt
  | Nil_trace
  and tr_elt =
  | Tr_Div of (nat * nat * nat)
  | Tr_DivZero
  | Tr_Jump of ident
  | Tr_JumpFail of ident
  | Tr_Pass of ident
  | Tr_Label of ident
  | Tr_AccessError of nat
  | Tr_MemUpdate of (nat * nat)
  | Tr_Sw of (nat * nat)
  | Tr_Lw of (nat * nat)
  | Tr_Addi of (nat * nat * nat)
  | Tr_Xor of (nat * nat * nat)
  | Tr_Add of (nat * nat * nat)
  | Tr_EnvUpdate of (nat * nat)
  and register =
  | R of nat
  and program =
  | Succ of (asm * program)
  | Nil
  and 'a option =
  | Some of 'a
  | None
  and instruction =
  | Sw of (register * register)
  | Lw of (register * register)
  | Addi of (register * register * immediate)
  | Xor of (register * register * register)
  | Div of (register * register * register)
  | Add of (register * register * register)
  and immediate =
  | Imm of nat
  and asm =
  | Bneq of (register * register * ident)
  | Beq of (register * register * ident)
  | Label of ident
  | Instr of instruction
  and monad = environment * memory -> value M.t
  and value = (environment * memory) option * trace

  val add: nat * nat -> nat M.t
  val add_instr: program * asm -> program M.t
  val bind: monad * (unit -> monad M.t) -> monad M.t
  val bind_v: value * (unit -> value M.t) -> value M.t
  val concat: program * program -> program M.t
  val div: nat * nat -> nat M.t
  val env_init: environment
  val eval_instr: instruction -> monad M.t
  val eval_prog: program * program -> monad M.t
  val exec: program -> value M.t
  val fail_v: value
  val find_label: ident * program -> ((program * program) option) M.t
  val find_label_aux: ident * program * program -> ((program * program) option) M.t
  val iseq: nat * nat -> unit M.t
  val isneq: nat * nat -> unit M.t
  val isnzero: nat -> unit M.t
  val iszero: nat -> unit M.t
  val log: tr_elt -> monad M.t
  val log_v: tr_elt -> value M.t
  val mem_init: memory
  val pass: monad
  val read_env: environment * nat -> nat M.t
  val read_mem: memory * nat -> (nat option) M.t
  val ret_v: environment * memory -> value M.t
  val streq: ident * ident -> unit M.t
  val strneq: ident * ident -> unit M.t
  val update_env: environment * nat * nat -> environment M.t
  val update_mem: memory * nat * nat -> memory M.t
  val xor: nat * nat -> nat M.t
end

(** Module defining the specified terms *)
module MakeInterpreter (F: UNSPEC) = struct
  include F

  let ( let* ) = M.bind

  let rec add_instr =
    function (p, asm_instr) ->
    let p' = Succ (asm_instr, Nil) in
    M.apply concat (p, p')
  and bind =
    function (m, f) ->
    M.ret (function (env, mem) ->
      let* (opt, tr) = M.apply m (env, mem) in
      begin match opt with
      | None -> M.ret (None, tr)
      | Some (env', mem') ->
          let* m' = M.apply f () in
          let* (opt', tr') = M.apply m' (env', mem') in
          M.ret (opt', Conc (tr, tr'))
      end)
  and bind_v =
    function (v, f) ->
    let (_, tr) = v in
    let* (opt', tr') = M.apply f () in
    M.ret (opt', Conc (tr', tr))
  and concat =
    function (p1, p2) ->
    begin match p1 with
    | Nil -> M.ret p2
    | Succ (asm_instr, p1') ->
        let* p' = M.apply concat (p1', p2) in
        M.ret (Succ (asm_instr, p'))
    end
  and eval_instr instr =
    begin match instr with
    | Addi (R rd, R rs, Imm i) ->
        let* _tmp = M.apply log (Tr_Addi (rd, rs, i)) in
        bind (_tmp, function _ ->
          M.ret (function (env, mem) ->
            let* n1 = M.apply read_env (env, rs) in
            let* n2 = M.apply add (n1, i) in
            let* env' = M.apply update_env (env, rd, n2) in
            let* _tmp = M.apply log_v (Tr_EnvUpdate (rd, n2)) in
            bind_v (_tmp, function () ->
              M.apply ret_v (env', mem))))
    | Add (R rd, R rs1, R rs2) ->
        let* _tmp = M.apply log (Tr_Add (rd, rs1, rs2)) in
        bind (_tmp, function _ ->
          M.ret (function (env, mem) ->
            let* n1 = M.apply read_env (env, rs1) in
            let* n2 = M.apply read_env (env, rs2) in
            let* n3 = M.apply add (n1, n2) in
            let* env' = M.apply update_env (env, rd, n3) in
            let* _tmp = M.apply log_v (Tr_EnvUpdate (rd, n3)) in
            bind_v (_tmp, function () ->
              M.apply ret_v (env', mem))))
    | Xor (R rd, R rs1, R rs2) ->
        let* _tmp = M.apply log (Tr_Xor (rd, rs1, rs2)) in
        bind (_tmp, function _ ->
          M.ret (function (env, mem) ->
            let* n1 = M.apply read_env (env, rs1) in
            let* n2 = M.apply read_env (env, rs2) in
            let* n3 = M.apply xor (n1, n2) in
            let* env' = M.apply update_env (env, rd, n3) in
            let* _tmp = M.apply log_v (Tr_EnvUpdate (rd, n3)) in
            bind_v (_tmp, function () ->
              M.apply ret_v (env', mem))))
    | Div (R rd, R rs1, R rs2) ->
        let* _tmp = M.apply log (Tr_Div (rd, rs1, rs2)) in
        bind (_tmp, function _ ->
          M.ret (function (env, mem) ->
            let* n1 = M.apply read_env (env, rs1) in
            let* n2 = M.apply read_env (env, rs2) in
            M.branch [
              (function () ->
                let* () = M.apply isnzero n2 in
                let* n3 = M.apply div (n1, n2) in
                let* env' = M.apply update_env (env, rd, n3) in
                let* _tmp = M.apply log_v (Tr_EnvUpdate (rd, n3)) in
                bind_v (_tmp, function () ->
                  M.apply ret_v (env', mem))) ;
              (function () ->
                let* () = M.apply iszero n2 in
                let* _tmp = M.apply log_v Tr_DivZero in
                bind_v (_tmp, function () ->
                  M.ret fail_v))]))
    | Sw (R rs, R rd) ->
        let* _tmp = M.apply log (Tr_Sw (rs, rd)) in
        bind (_tmp, function _ ->
          M.ret (function (env, mem) ->
            let* n = M.apply read_env (env, rs) in
            let* addr = M.apply read_env (env, rd) in
            let* mem' = M.apply update_mem (mem, addr, n) in
            let* _tmp = M.apply log_v (Tr_MemUpdate (addr, n)) in
            bind_v (_tmp, function () ->
              M.apply ret_v (env, mem'))))
    | Lw (R rd, R rs) ->
        let* _tmp = M.apply log (Tr_Lw (rd, rs)) in
        bind (_tmp, function _ ->
          M.ret (function (env, mem) ->
            let* addr = M.apply read_env (env, rs) in
            let* n = M.apply read_mem (mem, addr) in
            begin match n with
            | None ->
                let* _tmp = M.apply log_v (Tr_AccessError addr) in
                bind_v (_tmp, function () ->
                  M.ret fail_v)
            | Some n' ->
                let* env' = M.apply update_env (env, rd, n') in
                let* _tmp = M.apply log_v (Tr_EnvUpdate (rd, n')) in
                bind_v (_tmp, function () ->
                  M.apply ret_v (env', mem))
            end))
    end
  and eval_prog =
    function (acc, prog) ->
    begin match prog with
    | Nil -> M.ret pass
    | Succ (asm_instr, prog') ->
        begin match asm_instr with
        | Instr instr ->
            let* _tmp = M.apply eval_instr instr in
            bind (_tmp, function _ ->
              let* acc' = M.apply add_instr (acc, asm_instr) in
              M.apply eval_prog (acc', prog'))
        | Label l ->
            let* _tmp = M.apply log (Tr_Label l) in
            bind (_tmp, function _ ->
              let* acc' = M.apply add_instr (acc, asm_instr) in
              M.apply eval_prog (acc', prog'))
        | Beq (R r1, R r2, l) ->
            M.ret (function (env, mem) ->
              let* n1 = M.apply read_env (env, r1) in
              let* n2 = M.apply read_env (env, r2) in
              M.branch [
                (function () ->
                  let* _ = M.apply iseq (n1, n2) in
                  let* p = M.apply concat (acc, prog) in
                  let* opt = M.apply find_label (l, p) in
                  begin match opt with
                  | None -> M.ret (None, Sgl (Tr_JumpFail l))
                  | Some (acc', prog') ->
                      let* m' =
                        let* _tmp = M.apply log (Tr_Jump l) in
                        bind (_tmp, function () ->
                          M.apply eval_prog (acc', prog'))
                      in
                      M.apply m' (env, mem)
                  end) ;
                (function () ->
                  let* _ = M.apply isneq (n1, n2) in
                  let* m' =
                    let* _tmp = M.apply log (Tr_Pass l) in
                    bind (_tmp, function () ->
                      let* acc' = M.apply add_instr (acc, asm_instr) in
                      M.apply eval_prog (acc', prog'))
                  in
                  M.apply m' (env, mem))])
        | Bneq (R r1, R r2, l) ->
            M.ret (function (env, mem) ->
              let* n1 = M.apply read_env (env, r1) in
              let* n2 = M.apply read_env (env, r2) in
              M.branch [
                (function () ->
                  let* _ = M.apply isneq (n1, n2) in
                  let* p = M.apply concat (acc, prog) in
                  let* opt = M.apply find_label (l, p) in
                  begin match opt with
                  | None -> M.ret (None, Sgl (Tr_JumpFail l))
                  | Some (acc', prog') ->
                      let* m' =
                        let* _tmp = M.apply log (Tr_Jump l) in
                        bind (_tmp, function () ->
                          M.apply eval_prog (acc', prog'))
                      in
                      M.apply m' (env, mem)
                  end) ;
                (function () ->
                  let* _ = M.apply iseq (n1, n2) in
                  let* m' =
                    let* _tmp = M.apply log (Tr_Pass l) in
                    bind (_tmp, function () ->
                      let* acc' = M.apply add_instr (acc, asm_instr) in
                      M.apply eval_prog (acc', prog'))
                  in
                  M.apply m' (env, mem))])
        end
    end
  and exec prog =
    let* m = M.apply eval_prog (Nil, prog) in
    M.apply m (env_init, mem_init)
  and fail_v = (None, Nil_trace)
  and find_label =
    function (l, p) ->
    let* opt = M.apply find_label_aux (l, Nil, p) in
    M.ret opt
  and find_label_aux =
    function (l, acc, p) ->
    begin match p with
    | Nil -> M.ret None
    | Succ (asm_instr, prog') ->
        begin match asm_instr with
        | Label l' ->
            M.branch [
              (function () ->
                let* () = M.apply streq (l, l') in
                let* acc' = M.apply add_instr (acc, asm_instr) in
                M.ret (Some (acc', prog'))) ;
              (function () ->
                let* () = M.apply strneq (l, l') in
                let* acc' = M.apply add_instr (acc, asm_instr) in
                M.apply find_label_aux (l, acc', prog'))]
        | _ ->
            let* acc' = M.apply add_instr (acc, asm_instr) in
            M.apply find_label_aux (l, acc', prog')
        end
    end
  and log trElt =
    M.ret (function (env, mem) ->
      M.ret (Some (env, mem), Sgl trElt))
  and log_v trElt = M.ret (None, Sgl trElt)
  and pass =
    function (env, mem) ->
    M.ret (Some (env, mem), Nil_trace)
  and ret_v =
    function (env, mem) ->
    M.ret (Some (env, mem), Nil_trace)
end