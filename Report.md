# Projet Interpréteur RISC-V

Axel Ferreol

## Scope

Le projet consiste à créer un interpréteur RISC-V.

J'ai fait le choix de pouvoir interpréter du code assembleur RISC-V. Cela permet de représenter en plus des labels et d'yfaire référence pour les auts dans le code.

Je n'ai représenté qu'un nombre limité d'instructions, les plus intéressantes. Dans la section **Pour aller plus loin**, je discute des implications de représenter les autres instructions.

Parmis les instructions, on distigue ainsi:
* les opérations arithmétiques : ```add, addi, div, xor```
* les opérations intéragissant avec la mémoire ```sw, lw```
* les branchement ```beq, bneq```
* les labels ```Label```


Ainsi, j'ai modélisé le langage comme ceci:

    type instruction =
    | Add (register,register,register)
    | Div (register,register,register)
    | Xor (register,register,register)
    | Addi (register,register,immediate)
    | Lw (register,register)
    | Sw (register,register)

    type asm = 
    | Instr instruction
    | Label ident
    | Beq (register,register,ident)
    | Bneq (register,register,ident)

    type program =
    | Nil 
    | Succ (asm,program)

Concernant la modélisation du langage, j'ai fait le choix de séparer les instructions qui modifient le flot d'exécution et celles qui ne le font pas. Ceci permet d'alléger le *pattern matching* lors du traitement des instructions. Cela n'a aucune incidence sur l'exprissivité du langage. 


Pour la faciliter d'écriture du code assembleur, j'ai créé des pseudo-instructions:

    let mov (r1,r2) =
        Addi (r1,r2,Imm 0)
    let dec (r) =
        Addi (r,r,Imm (-1))
    let inc (r) =
        Addi (r,r,Imm (1))
    let jump(l)=
        Beq (R 0, R 0, l)
    let zero(r) =
        Xor (r,r,r)

J'ai aussi créé l'operateur ```@::``` pour remplacer le constructeur ```Succ``` et les multiples parenthèses qu'il induit.

Ainsi, on peut coder la fonction ```fibonaci```.
Cette focntion stocke dans la mémoire tous les nombres de la suite fibonaci, jusqu'au n-ème qu'il renvoie.

    let fibonaci n =
        Instr (zero (R 1)) @:: (*R1 : addr*)
        Instr (zero (R 2)) @:: (*R2 : values*)
        Instr (zero (R 3)) @:: (*R3 : n*)
        Instr (Addi (R 3, R 3, Imm n)) @::
        
        Instr (Sw (R 2, R 1)) @:: (*@0 <- 0*)
        Beq (R 3, R 1, "result") @::
        Instr (inc(R 1)) @:: 
        Instr (inc(R 2)) @::
        Instr (Sw (R 2, R 1)) @:: (*@1 <- 1*)
        Beq (R 3, R 1, "result") @::
        Instr (inc(R 1)) @::
        Instr (Sw (R 2, R 1)) @:: (*@2 <- 1*)
        
        Label "loop" @::
        Beq (R 3, R 1, "result") @::
        Instr (dec(R 1)) @::
        Instr (Lw (R 4, R 1)) @:: (*x4 <- fib(addr-2) *)
        Instr (inc (R 1)) @::
        Instr (Lw (R 5, R 1)) @:: (*x5 <- fib(addr-1) *)
        Instr (Add (R 5, R 5, R 4)) @::
        Instr (inc (R 1)) @::
        Instr (Sw (R 5, R 1)) @:: (*@addr <- fib(addr) *)
        jump ("loop") @::

        Label "result" @::
        Instr (Lw (R 2, R 1)) @::
        Instr (mov (R 1, R 2)) @::
        Nil


## Modèle

J'ai fait le choix de modéliser:
* les registres du microprocesseurs
* la mémoire (à la fois la *stack* et la *heap*, c'est au programmeur de gérer l'utilisation de la mémoire)
* la possibilité d'un crash du microprocesseur (une division par zero, ou un saut à un label n'existant pas, par exemple)
* une trace pour écrire les *logs*

Les registres du microprocesseur sont représenté par le type ```environment``` qui garde la valeur de chaque registre. Concrètement, ```environment``` est implémenté comme une fonctions de type ```registre -> valeur ```. A noter que le registre **x0** est hard-codé à 0 !

La mémoire est représentée par le type ```memory``` qui contient le contenu de chaque adresse mémoire. Pour l'intérêt du projet (et donc l'utilisation de monades), je n'autorise que la lecture à une adresse qui a déjà été écrite. Ainsi, ```memory``` est implémenté comme une fonction de type ```adresse -> valeur option``` Le type ```option``` permet de dire qu'une lecture en mémoire a échouée (car aucune valeur n'avait déjà été écrite)

La trace est représneté par une liste d'éléments, comme vu en cours.

De plus, l'exécution du programme n'a de sens que si on lui fournit un environment et une mémoire initiaux (comme vu en cours).

On obtient ainsi le type de notre monade ```(environment, memory) -> (value, trace)```, avec ```type value = option<(environment, memory)> ```

L'idée est de dire que l'execution d'une instruction pour un contexte (environement et mémoire) donné, produit une erreur ou un nouveau contexte (d'où le type option) et une trace de ce qui s'est passé. On considère que le résultat du code doit être stocké dans le registre **x1**, celui qui sera affiché par l'interpréteur

La gestion des branchements est assurée par la fonction ```eval_prog``` qui choisit la prochaine instruction à exécuter.
La gestion des autres instructions est assurée par la fonction ```eval_instr```.

## Exécution

La fonction ```eval_prog``` évalue le programme. Si le programme est vide, elle termine, s'il s'agit d'une instruction (autre que branchement), elle l'exécute puis exécute le reste du programme, enfin, s'il s'agit d'un branchement, elle cherche la prochaine instruction à exécuter.
```eval_prog``` ne modifie pas le contexte ```(environment, memory)```, seulement le programme. Pour ce faire, elle représente le programme comme une liste d'instructions antériueures et une liste d'instructions postérieures (les notions d'antérieur et postérieur font référence à la position des instructions dans le programme et non à leur ordre d'exécution). Cela permet dobtenir le programme total, en concaténant les deux listes, pour y chercher un *Label*, puis de le rediviser à la postion du label pour continuer l'exécution.

La recherche du *Label* peut échouer , dans ce cas l'évaluation du programme échoue également. La recherche du *Label* se résume à rechercher un élément dans une liste, c'est fait par une fonction annexe.

La traitement des instructiosn de branchement nécessite de comparer la valeur des registres. Suivant le résultat, un saut sera fait on non. Ce comportement est modélisé par l'utilisation de ```branch .. or .. end``` selon un critère d'égalité des registres.

La fonction ```eval_instr``` exécute les instructions autres que branchement. Elle modifie l'environement ou la mémoire selon l'instruction. Elle peut également échouer en cas de division par zéro ou d'accès illégal en mémoire. Ces compotements sont modélisés par l'emploi de structure ```branch .. or .. end```.

Une opération récurrente consiste à donner le contexte, résultant de l'évaluation d'une première instruction, à la prochaine instruction, et d'écrire les logs. Ceci est réalisé par la fonction ```bind```.


Une autre opération fréquente est de devoir revoyer une ```value``` *fail* ou *ret* puis de l'enrichir. Ceci est modélisé par la fonction ```bind_v```. 

## Pour aller plus loin

Pour représenter d'autres instructions arithmétiques, il n'y a pas de difficulté particulière: on s'inspire de ce qui a été fait pour *add* ou *div*.

Pour représenter d'autres instuctions de branchement, on s'inspire également de ce qui a déjà était fait pour *beq* ou *bneq*.

Pour représenter l'instruction **ret**, on peut vouloir stocker le nom du label dans un registre. Cela implique de modifier l'environement, qui pourrait désormais, soit stocker des entiers soit des noms de labels. Une autre solution serait de modifier les labels pour qu'ils soient désormais numérotés et non plus nommés. Cela ne représente pas de grandes difficultés.

## Pour exécuter

Simplement
    dune exec R5
